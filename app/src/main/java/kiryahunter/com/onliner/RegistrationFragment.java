package kiryahunter.com.onliner;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Администратор on 29.03.2015.
 */
public class RegistrationFragment extends Fragment {
    OnFragmentButtonClickListener listener;
    Button buttonRegMe;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnFragmentButtonClickListener)activity;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.registration_fragment,container,false);//super.onCreateView(inflater, container, savedInstanceState);
        buttonRegMe = (Button)rootView.findViewById(R.id.reg_fragment_button_reg_me);
        buttonRegMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onButtonRegMeClick();
            }
        });
        //////////////////////////
        ImageView imageView = (ImageView)rootView.findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate);
        imageView.startAnimation(animation);
        return rootView;
    }

    public interface OnFragmentButtonClickListener{
        void onButtonRegMeClick();
    }
}
