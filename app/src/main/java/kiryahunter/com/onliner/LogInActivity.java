package kiryahunter.com.onliner;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class LogInActivity extends Activity implements LogInFragment.OnFragmentButtonClickListener, RegistrationFragment.OnFragmentButtonClickListener{

    LogInFragment fragmentLogIn; RegistrationFragment fragmentRegistration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        FragmentManager manager = getFragmentManager();
        fragmentLogIn = new LogInFragment();
        if (manager.findFragmentById(R.id.layout_for_fragments)==null){
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.add(R.id.layout_for_fragments,fragmentLogIn);
            fragmentTransaction.commit();
        }



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLogInButtonClick() {
    //обработчик нажатия кнопни LogIn
        Intent intent = new Intent(LogInActivity.this,MainDialogsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRegistrationButtonClick() {
    //Обработчик перехода на фрагмент регистрации
        fragmentRegistration = new RegistrationFragment();
       FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_for_fragments,fragmentRegistration);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onButtonRegMeClick() {
        //Обработчик регистрации
    }
}
