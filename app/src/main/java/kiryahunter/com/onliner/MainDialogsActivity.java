package kiryahunter.com.onliner;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;


public class MainDialogsActivity extends Activity {
    ActionBar bar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dialogs);
        //Создаем разметку поиска контактов для ActionBar, обрабатываем нажатие поисковой кнопки
        bar = getActionBar();
        if (bar !=null) {
            //Toast.makeText(getApplicationContext(),String.valueOf(bar.getDisplayOptions()),Toast.LENGTH_LONG).show();
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.seek_action_bar);
            ImageButton buttonSeek = (ImageButton) findViewById(R.id.seek_action_bar_button_seek);
            buttonSeek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bar.setDisplayOptions(11);
                    bar.setTitle(R.string.app_name);
                }
            });
        }
        /////////////////////////////

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_dialogs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
