package kiryahunter.com.onliner;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Администратор on 29.03.2015.
 */
public class LogInFragment extends Fragment {
    OnFragmentButtonClickListener listener;
    Button buttonLogIn, buttonRegistration;
    EditText editTextPhone, editTextPassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.log_in_fragment,container,false);//super.onCreateView(inflater, container, savedInstanceState);
        buttonLogIn = (Button)rootView.findViewById(R.id.button_log_in);
        buttonRegistration = (Button)rootView.findViewById(R.id.button_registration);
        editTextPhone = (EditText)rootView.findViewById(R.id.log_in_fragment_phone);
        editTextPassword = (EditText)rootView.findViewById(R.id.log_in_fragment_password);
        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onLogInButtonClick();
            }
        });
        buttonRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRegistrationButtonClick();
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnFragmentButtonClickListener)activity;
        //здесь я хз, че вообще за синтаксис =))
    }

    public interface OnFragmentButtonClickListener{
        void onLogInButtonClick();
        void onRegistrationButtonClick();
    }

}
